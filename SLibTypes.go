/*
	### This file is a part of SLib created by snixtho ###
*/

package S

/*
	Types
*/

// unsigned integers
type UI8 uint8
type UI16 uint16
type UI32 uint32
type UI64 uint64

// signed integers
type I8 int8
type I16 int16
type I32 int32
type I64 int64

// floating points
type F float32
type F64 float64

// complex numbers
type CX complex64
type CX128 complex128

// bytes and runes
type B byte
type R rune

// strings
type S string


/*
	Arrays
*/

// unsigned integers
type UI8A []uint8
type UI16A []uint16
type UI32A []uint32
type UI64A []uint64

// signed integers
type I8A []int8
type I16A []int16
type I32A []int32
type I64A []int64

// floating points
type FA []float32
type F64A []float64

// complex numbers
type CA []complex64
type C128A []complex128

// bytes and runes
type BA []byte
type RA []rune

// integers and pointers
type UIA []uint
type IA []int
type PA []uintptr

// strings
type SA []string

/*
	Maps
*/

// string key
type MSS map[string]string
type MSI map[string]int
type MSF map[string]float32
type MSB map[string]byte
type MSR map[string]rune

// integer key
type MIS map[int]string
type MII map[int]int
type MIF map[int]float32
type MIB map[int]byte
type MIR map[int]rune