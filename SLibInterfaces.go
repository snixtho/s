/*
	### This file is a part of SLib created by snixtho ###
*/

package S

/*
	String array to interface array.
*/
func SAToIntfA(args []string) ([]interface{}) {
	intf := make([]interface{}, len(args))
	
	for i, _ := range args {
		intf[i] = args[i]
	}
	
	return intf
}